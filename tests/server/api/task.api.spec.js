/*jshint expr: true*/
'use strict';

const expect    = require('chai').expect;
const mongoose  = require('mongoose');
const supertest = require('supertest');
const fixtures  = require('node-mongoose-fixtures');

const Task      = require('../../../server/models/task');
const app       = require('../../../server/app');
const dbUrl     = require('../../../server/config/server.config').mongoConfig.test.url;

const clearDb   = require('mocha-mongoose')(dbUrl, {noClear: true});


describe('Task api', () => {

  before((done) => {
    // connection to mongoose exsists do nothing and return.
    if(mongoose.connection.db) return done;

    // Else connect to mongoose, with url given in config file
    mongoose.connect(dbUrl, done);
  });

  before((done) => {
    // Load fixtures for this test
    fixtures( require('../fixtures/many.tasks'), done );
  });

  after((done) => {
    // Nuke the DB after all tests
    clearDb(done);
  });

  // TESTS: --------------------------------------------------------------------
  it('#GET should return all tasks', (done) => {
    supertest(app)
    .get('/api/task')
    .expect('content-type', /json/)
    .expect(200)
    .end((err, res) => {
      if(err) throw err;

      expect(res.body.length).to.equal(2);
      done();
    });
  });

  it('#POST should create a new record', (done) => {
    supertest(app)
    .post('/api/task')
    .send({ text: 'posted task'})
    .expect('content-type', /json/)
    .expect(200)
    .end( (err, res) => {
      if(err) throw err;

      expect(res).deep.property('body.success', 'Task created');
      expect(res).deep.property('body.task.text', 'posted task');
      expect(res.body.task.__v).to.be.undefined;
      expect(res.body.task._id).to.be.ok;
      // Check the db if the task is saved
      Task.find({text: 'posted task'})
      .then( (tasks) => {
        expect(tasks[0]._id).to.be.ok;
      })
      .then(done);
    });
  });

  it('#DELETE should remove a record', (done) => {
    supertest(app)
    .delete('/api/task/500000000000000000000001')
    .expect('content-type', /json/)
    .expect(200)
    .end( (err, res) => {
      if(err) throw err;
      expect(res.body).property('success', 'Task deleted');

      // Test if the document deleted from mongo
      Task.findById('500000000000000000000001')
      .then( (doc) => {
        expect(doc).to.not.be.ok;   // record should not exist
      })
      .then(done);
    });
  });

  it('#DELETE on a non existant record should be a 404', (done) => {
    supertest(app)
    .delete('/api/task/500000000000000000000009')
    .expect('content-type', /json/)
    .expect(404)
    .expect({ error: 'Task not found'})
    .end(done);
  });


  it('#GET with id should return a single task', (done) => {
    supertest(app)
    .get('/api/task/500000000000000000000002')
    .expect('content-type', /json/)
    .expect(200)
    .end( (err, res) => {
      if(err) throw err;

      expect(res.body).property('text', 'Task 2');
      expect(res.body).property('status', false);
      done();
    });
  });

  it('#PUT should update a document', (done) => {
    supertest(app)
    .put('/api/task/500000000000000000000002')
    .send({ text: 'updated task' })
    .expect('content-type', /json/)
    .expect(200)
    .end( (err, res) => {
      if(err) throw err;

      expect(res.body).property('success', 'Updated task');

      // check db for updated task
      Task.findById('500000000000000000000002')
      .then( (task) => {
        expect(task).property('text', 'updated task')
      })
      .then(done);
    });
  });

  it('#PUT on non-existant record should be a 404', (done) => {
    supertest(app)
    .put('/api/task/500000000000000000000009')
    .expect('content-type', /json/)
    .expect(404)
    .expect({ error: "Task not found"})
    .end(done);
  });
});
