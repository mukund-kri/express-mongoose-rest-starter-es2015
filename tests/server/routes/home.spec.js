'use strict';

const expect    = require('chai').expect;
const supertest = require('supertest');

const app       = require('../../../server/app');


describe('#Home routes', () => {
  it('#Home page should render correctly', (done) => {
    supertest(app)
    .get('/')
    .expect('content-type', /text/)
    .expect(200)
    .end( (err, res) => {
      if(err) throw err;

      expect(res.text).to.match(/Your\ App/);
      done();
    });
  });
});
