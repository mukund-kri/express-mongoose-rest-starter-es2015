'use strict';

const faker = require('faker');

var genTaskData = function() {
  return {
    text: faker.lorem.sentence(6)
  };
};

module.exports = {
  genTaskData
};
