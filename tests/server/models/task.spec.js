/*jshint expr: true*/
'use strict';

const expect      = require('chai').expect;
const mongoose    = require('mongoose');

const Task        = require('../../../server/models/task');
const fakerHelper = require('./faker.helper');
const dbUrl       = require('../../../server/config/server.config').mongoConfig.test.url;
const clearDb     = require('mocha-mongoose')(dbUrl, {noClear: true});


describe('Task Model', () => {

  before((done) => {
    if(mongoose.connection.db) return done();
    mongoose.connect(dbUrl, done);
  });

  after((done) => {
    clearDb(done);
  });

  it('#Instatiate and save', (done) => {
    let task = new Task({ text: 'Task 1'});
    task.save()
    .then((doc) => {
      expect(doc.__v).to.not.be.undefined;
      expect(doc).property('status', false);
    })
    .then(done);

  });

  it('#Can be created', (done) => {
    let fakeTaskData = fakerHelper.genTaskData();

    Task.create(fakeTaskData)
    .then((doc) => {
      expect(doc.__v).to.not.be.undefined;
      expect(doc).property('text', fakeTaskData.text);
    })
    .then(done);
  });

  it('#Records can be listed', (done) => {
    Task.find({})
    .then( (docs) => {
      expect(docs.length).to.equal(2);
    })
    .then(done);
  });

  // TODO: You know the drill, add your tests here ...
});
