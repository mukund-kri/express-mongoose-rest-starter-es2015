'use strict';


const gulp   = require('gulp');
const server = require('gulp-develop-server');
const mocha  = require('gulp-spawn-mocha');
const runSeq = require('run-sequence');
const gutil  = require('gulp-util');
const clean  = require('gulp-clean');
const path   = require('path');
const jshint = require('gulp-jshint');

const SERVER_CODE = './server/**/*.js';
const TEST_DIR = './tests/server';
const TEST_CODE = TEST_DIR + '/**/*.js';


const JSHINT_CONFIG = {
  esversion: 6,
  node: true
};

const JSHINT_TEST_CONFIG = {
  esversion: 6,
  node: true,
  mocha: true
};


/* ----------------------------- CHECKING / STYLE --------------------------- */
gulp.task('lint:servercode', () => {
  return gulp.src(SERVER_CODE)
    .pipe(jshint(JSHINT_CONFIG))
    .pipe(jshint.reporter('default'));
});

gulp.task('lint:tests', () => {
  return gulp.src(TEST_CODE)
    .pipe(jshint(JSHINT_TEST_CONFIG))
    .pipe(jshint.reporter('default'));
});

gulp.task('lint', ['lint:servercode', 'lint:tests']);

/* ------------------------------ DEV TASKS --------------------------------- */
// Use gulp-develop-server to run the express in development mode
gulp.task('server:start', () => {
  server.listen({ path: './server/develop.js' });
});

gulp.task('server:watch', () => {
  gulp.watch(SERVER_CODE, server.restart);
});

// Run all development tasks
gulp.task('develop', ['server:start', 'server:watch']);

// The default task is just an alias for develop
gulp.task('default', ['develop']);

/* ----------------------------- TESTING TASKS ------------------------------ */
// test models
gulp.task('test:models', () => {
  gulp.src(TEST_DIR + "/models/*.spec.js")
  .pipe(mocha());
});

// test routes
gulp.task('test:routes', () => {
  gulp.src(TEST_DIR + "/routes/*.spec.js")
  .pipe(mocha());
});

// test api
gulp.task('test:api', () => {
  gulp.src(TEST_DIR + "/api/*.spec.js")
  .pipe(mocha());
});

// Run all tests
gulp.task('test', () => runSeq('test:models', 'test:routes', 'test:api'));
