'use strict';


const path       = require('path');

// Make bluebird the promise provider for mongoose
const mongoose   = require('mongoose');
mongoose.Promise = require('bluebird');

// Express config
const expressConfig = {
  'views':       path.resolve(__dirname, '../', 'views'),
  'view engine': 'pug'
};

// mongo configuration
const mongoConfig = {
  dev: {
    url: 'mongodb://localhost/devdb',
  },
  test: {
    url: 'mongodb://localhost/testdb'
  }
};

module.exports = {
  expressConfig,
  mongoConfig
};
