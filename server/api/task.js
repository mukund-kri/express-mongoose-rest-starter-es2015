'use strict';


var Promise = require('bluebird')
function wrap (genFn) { // 1
  var cr = Promise.coroutine(genFn) // 2
  return function (req, res, next) { // 3
    cr(req, res, next).catch(next);
  };
};

const express     = require('express');
const createError = require('http-errors');

const Task        = require('../models/task');


var router = express.Router();

// Task listing
router.get('/', wrap(function* (req, res) {
  let tasks = yield Task.find({});
  res.json(tasks);
}));

// Get a single task
router.get('/:id', wrap(function* (req, res, next) {
  let docId = req.params.id;
  let task = yield Task.findById(docId);
  if(!task) throw createError(404, 'Task not found');
  else res.json(task);
}))

// Create a new task
router.post('/', wrap(function* (req, res, next) {
  let task = new Task({
    text: req.body.text
  });
  yield task.save();
  res.json({ success: 'Task created', task: task });
}));

// Delete a task
router.delete('/:id', wrap(function* (req, res, next) {
  let docId = req.params.id;
  let task = yield Task.findByIdAndRemove( docId );

  if( !task ) throw createError(404, 'Task not found');
  else res.json({ success: 'Task deleted'});
}));

// Update a task
router.put('/:id', wrap(function* (req, res, next) {
  let docId = req.params.id;

  let updateQuery = { $set: {} };
  for(let key in req.body) {
    updateQuery.$set[key] = req.body[key]
  }
  let doc = yield Task.findByIdAndUpdate(docId, updateQuery);

  if(!doc) throw createError(404, 'Task not found');
  else res.json({ success: 'Updated task' });
}));

// Error handling for this api
router.use((err, req, res, next) => {

  if(err instanceof createError.NotFound) {
    // resource not found
    res.status(err.statusCode).json({ error: err.message });
  } else {
    // return a 500 for every other exception.
    res.status(500).json({ error: 'Internal Error'});
  }
});

module.exports = router;
