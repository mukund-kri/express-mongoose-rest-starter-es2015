const mongoose = require('mongoose');

const app      = require('./app');
const config   = require('./config/server.config');


mongoose.connect(config.mongoConfig.dev.url);

// Run the server on port 4000
app.listen(4000);
