'use strict';

const replacer = function(key, value) {
  if(key == '__v') {
    return undefined;
  } else {
    return value;
  }
};

module.exports = {
  replacer
};
