# Express+Mongoose REST api in ES2015 starter project #

A starter project for writing REST api with ExpressJS and MongooseJS, witten
in ES2015.

Note: Only the language features supported by node 6.x is used. For full
blown, latest language features project look at the es2017 project.


## Features ##

 1. Vagrant

 1. node 6

 I use Node 6.x in this vagrant. The code in this project is on the edge of
 what node supports and I'm sure you will run into problems with anything less
 than node 6.

 1. List of Frameworks/Libs
  * ExpressJS
  * MongooseJS
  * mocha
  * chai
  * gulp
